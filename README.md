# Failure mode analysis for the Real Food Campaign Lab

We are just starting failure mode analysis, so it is quite incomplete.

We have two seperate work sheets:


* [**Manufacturing**](https://our-sci.gitlab.io/real-food-campaign/labs/lab-failure-mode-analysis/manufacturing.html) - for failures that happen and can be detected during the manufacturing process, thus will not affect the end user.
* [**Use**](https://our-sci.gitlab.io/real-food-campaign/labs/lab-failure-mode-analysis/use.html) - for failures that occur when the reflectometer / bionutrient meter is with the end user.


Both worksheets are written as CSV files are validated against a JSON schema. This allows simple interaction with the data via spreadsheet software, but also allows us to check the data integrity and check the files into Git as text for posterity.
